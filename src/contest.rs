//! Fetching and compilation functions for contests

use std::collections::HashMap;

use crate::problem::*;

use crate::result::MapmErr;
use crate::result::MapmErr::*;

use crate::template::fetch_template_config;
use crate::template::Template;

use crate::utils::copy::copy_dir_ignore_dots;

use std::env;
use std::fs;
use std::path::Path;
use std::process::Command;

use serde::{Deserialize, Serialize};

pub struct Contest {
    pub template: Template,
    pub problems: Vec<Problem>,
    pub problem_count: Option<u32>,
    pub vars: HashMap<String, String>,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct SerializedContest {
    template: String,
    problems: Vec<String>,
    problem_count: Option<u32>,
    vars: HashMap<String, String>,
}

/// The result you get from fetching a contest

pub type ContestFetchResult = Result<Contest, ContestFetchErrs>;

#[derive(Debug)]
pub struct ContestFetchErrs {
    pub contest_err: Option<MapmErr>,
    pub problem_errs: Option<Vec<(String, Vec<MapmErr>)>>,
}

pub struct ContestCompileResults {
    pub err: Option<MapmErr>,
    pub latexmk_results: Vec<Result<String, String>>,
}

/// Parses contest yaml into full-fledged contest, reading problem yamls based on `problem_dir` from the corresponding problem names in the contest file
///
/// MapmResult<Contest> will return with a ContestErr if the contest yaml cannot be parsed or does not satisfy the template conditions
///
/// Option<Vec<MapmErr>> will return with Some if any of the problems do not satisfy the template conditions, with the problem name being matched to the errors

fn parse_contest_yaml<P: AsRef<Path>, Q: AsRef<Path>>(
    yaml: &str,
    problem_dir: P,
    template_dir: Q,
) -> ContestFetchResult {
    let problem_dir: &Path = problem_dir.as_ref();
    let template_dir: &Path = template_dir.as_ref();
    match serde_yaml::from_str::<SerializedContest>(yaml) {
        Ok(serialized_contest) => {
            match fetch_template_config(&serialized_contest.template, template_dir) {
                Ok(template) => {
                    let problem_count_check: Option<u32> = match serialized_contest.problem_count {
                        Some(count) => Some(count),
                        None => template.problem_count,
                    };
                    if problem_count_check
                        != Some(serialized_contest.problems.len().try_into().unwrap())
                        && template.problem_count != None
                    {
                        return Err(ContestFetchErrs {
                            contest_err: Some(TemplateErr(String::from("The number of problems in the contest yaml is not equivalent to the specified `problem_count`"))),
                            problem_errs: None
                        });
                    }

                    let mut problems: Vec<Problem> = Vec::new();
                    let mut problem_errs: Vec<(String, Vec<MapmErr>)> = Vec::new();

                    for problem_name in serialized_contest.problems {
                        let problem = fetch_problem(&problem_name, problem_dir);
                        match problem {
                            Ok(problem) => {
                                problems.push(problem);
                            }
                            Err(err) => {
                                let errs = vec![err];
                                problem_errs.push((problem_name, errs));
                            }
                        }
                    }
                    for problem in &problems {
                        match problem.check_template(&template) {
                            Some(errs) => {
                                problem_errs.push((problem.name.clone(), errs));
                            }
                            None => {}
                        }
                    }

                    let contest = Contest {
                        template,
                        problems,
                        problem_count: serialized_contest.problem_count,
                        vars: serialized_contest.vars,
                    };

                    let problem_errs_opt: Option<Vec<(String, Vec<MapmErr>)>> =
                        if problem_errs.is_empty() {
                            None
                        } else {
                            Some(problem_errs)
                        };

                    if matches!(contest.check_template(), None) && matches!(problem_errs_opt, None)
                    {
                        return Ok(contest);
                    }

                    Err(ContestFetchErrs {
                        contest_err: None,
                        problem_errs: problem_errs_opt,
                    })
                }
                Err(err) => Err(ContestFetchErrs {
                    contest_err: Some(err),
                    problem_errs: None,
                }),
            }
        }
        Err(err) => Err(ContestFetchErrs {
            contest_err: Some(ContestErr(err.to_string())),
            problem_errs: None,
        }),
    }
}

/// Gets a contest from a contest path, problem directory, and template directory.

pub fn fetch_contest<P: AsRef<Path>, Q: AsRef<Path>, R: AsRef<Path>>(
    contest_path: P,
    problem_dir: Q,
    template_dir: R,
) -> ContestFetchResult {
    let contest_path: &Path = contest_path.as_ref();
    match fs::read_to_string(contest_path) {
        Ok(contest_yaml) => parse_contest_yaml(&contest_yaml, problem_dir, template_dir),
        Err(_) => Err(ContestFetchErrs {
            contest_err: Some(ContestErr(format!(
                "Could not read contest from {:?}",
                contest_path,
            ))),
            problem_errs: None,
        }),
    }
}

impl Contest {
    /// Check whether the contest has the proper vars defined, and each problem has the proper problemvars and solutionvars defined

    pub fn check_template(&self) -> Option<Vec<MapmErr>> {
        let mut mapm_errs: Vec<MapmErr> = Vec::new();

        for var in &self.template.vars {
            if !self.vars.contains_key(var) {
                mapm_errs.push(ContestErr(format!("Does not contain key `{}`", &var)));
            }
        }
        if !mapm_errs.is_empty() {
            Some(mapm_errs)
        } else {
            None
        }
    }
    /// Compiles the contest given a template directory to query

    pub fn compile<T: AsRef<Path>>(&self, template_dir: T) -> ContestCompileResults {
        let template_dir: &Path = template_dir.as_ref();
        let cwd = env::current_dir().unwrap();
        let mut latexmk_results: Vec<Result<String, String>> = Vec::new();

        let template_path = &template_dir.join(&self.template.name);
        let build_dir = Path::new("build");
        if !build_dir.is_dir() {
            // If `build` exists but is not a directory, user intervention is required - so we panic.
            if build_dir.exists() {
                panic!(
                    "{:?} exists in {:?} but is not a directory; please move it to fix this issue.",
                    build_dir, cwd,
                );
            }
            fs::create_dir(&build_dir).unwrap_or_else(|_| {
                panic!("Could not create directory {:?} in {:?}", build_dir, cwd)
            });
        }

        if !template_path.exists() {
            return ContestCompileResults {
                err: Some(TemplateErr(format!(
                    "Could not read template {:?}",
                    template_path
                ))),
                latexmk_results,
            };
        }

        copy_dir_ignore_dots(template_path, Path::new("build")).unwrap();

        env::set_current_dir(&build_dir)
            .unwrap_or_else(|_| panic!("Could not set current directory to {:?}", &build_dir));

        let mut headers = String::new();

        for (key, val) in &self.vars {
            headers.push_str(&format!(
                "\\expandafter\\def\\csname mapm@var@{}\\endcsname{{{}}}\n",
                key, val,
            ));
        }

        headers.push_str(&write_as_tex(&self.problems));

        fs::write("mapm-headers.tex", headers).unwrap_or_else(|_| {
            panic!(
                "Could not write `mapm-headers.tex` to directory {:?}",
                template_dir
            )
        });

        for (tex_path, unparsed_pdf_path) in &self.template.texfiles {
            let left_braces: Vec<usize> = unparsed_pdf_path
                .match_indices('{')
                .map(|(i, _)| i)
                .collect();
            let right_braces: Vec<usize> = unparsed_pdf_path
                .match_indices('}')
                .map(|(i, _)| i)
                .collect();
            if left_braces.len() != right_braces.len() {
                return ContestCompileResults {
                    err: Some(TemplateErr(format!(
                        "Unbalanced braces for the value `{}` of key `{}` in the `texfiles` of template `{}`",
                        unparsed_pdf_path,
                        tex_path,
                        &self.template.name,
                    ))),
                    latexmk_results,
                };
            }

            let mut parsed_pdf_path = String::new();

            // We reuse this errmsg, so define it here
            let nested_braces_error = format!("Nested braces are not supported. See the value `{}` of key `{}` in the `texfiles` of template `{}`", unparsed_pdf_path, tex_path, &self.template.name);

            if !left_braces.is_empty() {
                for n in 0..left_braces.len() {
                    if left_braces[n] > right_braces[n] {
                        return ContestCompileResults {
                            err: Some(TemplateErr(nested_braces_error)),
                            latexmk_results,
                        };
                    }
                }

                for n in 0..left_braces.len() - 1 {
                    if right_braces[n] > left_braces[n + 1] {
                        return ContestCompileResults {
                            err: Some(TemplateErr(nested_braces_error)),
                            latexmk_results,
                        };
                    }
                }

                for n in 0..left_braces.len() {
                    let key = &unparsed_pdf_path[left_braces[n] + 1..right_braces[n]];
                    match self.vars.get(key) {
                        Some(val) => {
                            if n == 0 {
                                parsed_pdf_path.push_str(&unparsed_pdf_path[..left_braces[n]]);
                            } else {
                                parsed_pdf_path.push_str(
                                    &unparsed_pdf_path[right_braces[n - 1] + 1..left_braces[n]],
                                );
                            }
                            parsed_pdf_path.push_str(val);
                        }
                        None => {
                            return ContestCompileResults {
                                err: Some(TemplateErr(
                                    format!(
                                        "The value `{}` of key `{}` in the `texfiles` key of template `{}` is referencing undefined template variable `{}`",
                                        unparsed_pdf_path,
                                        tex_path,
                                        self.template.name,
                                        key,
                                    )
                                )),
                                latexmk_results,
                            };
                        }
                    }
                }

                parsed_pdf_path
                    .push_str(&unparsed_pdf_path[right_braces[right_braces.len() - 1] + 1..]);
            } else {
                parsed_pdf_path.push_str(unparsed_pdf_path);
            }
            let latexmk = Command::new("latexmk").args([&format!("-pdflatex={}", &self.template.engine), "-pdf", "-f", tex_path]).output().expect("Failed to execute latexmk.\nMake sure you have the latexmk script installed\nand make sure it is in your PATH.");
            if latexmk.status.success() {
                latexmk_results.push(Ok(format!("Finished compiling `{}`", &parsed_pdf_path)));
                let original_pdf_path = Path::new(tex_path).with_extension("pdf");

                fs::rename(&original_pdf_path, &cwd.join(&parsed_pdf_path)).unwrap_or_else(|_| {
                    panic!(
                        "Could not rename {:?} to `{}`",
                        &original_pdf_path, &parsed_pdf_path
                    )
                });
            } else {
                latexmk_results.push(Err(format!("Failed to compile {}", &parsed_pdf_path)));
            }
        }

        env::set_current_dir(&cwd)
            .unwrap_or_else(|_| panic!("Failed to set current working directory to {:?}", cwd));

        ContestCompileResults {
            err: None,
            latexmk_results,
        }
    }
}
