//! # mapm
//!
//! `mapm` is a library that allows problem yaml files to be collated into a contest file and compiled with a user-defined template.
//!
//! There is a command-line utility as well as a graphical utility for mapm.
//!
//! ## Requirements
//!
//! A working version of `latexmk`, as well as a properly configured `~/.latexmkrc` file, are required.

pub mod contest;
pub mod problem;
pub mod result;
pub mod sty;
pub mod template;
mod utils;
